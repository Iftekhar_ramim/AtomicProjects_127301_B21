<?php

namespace App\bitm\seip_127301\hobby;
use  App\bitm\seip_127301\message\Message;
use  App\bitm\seip_127301\utility\Utility;

class Hobby {
    public $id='';
    public $name='';
    public $hobby='';
    public $conn='';

    public $deleted_at='';


    public function __construct()
    {
        $this->conn = new \mysqli("localhost","root","","atomicproject") or die("Database Connection Establish Fail.");
    }

    public function prepare($data=Array()){
        if(array_key_exists("name",$data)){
            $this->name= $data['name'];
        }

        if(array_key_exists("Hobby",$data)){
            $this->hobby= $data['Hobby'];
        }

        if(array_key_exists("id",$data)){
            $this->id= $data['id'];
        }

        //Utility::dd($this->name);
        return $this;

    }

    public function index()
    {
        $allhobby= array();
        $query= "SELECT * FROM `hobby` WHERE `deleted_at` IS NULL ";
        $result= mysqli_query($this->conn,$query);

        while ($row=mysqli_fetch_object($result)){
            $allhobby[] = $row;
        }

        return $allhobby;

    }

    public function view()
    {
        $query= "SELECT * FROM `hobby` WHERE `id`='{$this->id}'";
        $result= mysqli_query($this->conn,$query);
        $row=mysqli_fetch_assoc($result);
        return $row;
    }

    public function store(){
        $query="INSERT INTO `atomicproject`.`hobby` ( `name`, `hobby`) VALUES ('{$this->name}','{$this->hobby}')";
        $result = $this->conn->query($query);
        //Utility::dd($result);
        if($result){
            Message::message("<div class=\"alert alert-info\">
  <strong>Inserted!</strong> Data has been inserted successfully.
</div>");
            Utility::redirect('index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been inserted  successfully.
    </div>");
            Utility::redirect('index.php');

        }
    }




    public function update()
    {
        $query="UPDATE `hobby` SET `name` = '{$this->name}', `hobby` = '{$this->hobby}' WHERE `hobby`.`id` =".$this->id;
        $result= mysqli_query($this->conn,$query);

        if($result){
            Message::message("<div class=\"alert alert-info\">
  <strong>Updated!</strong> Data has been Updated successfully.
</div>");
            Utility::redirect('index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been updated  successfully.
    </div>");
            Utility::redirect('index.php');

        }

    }

    public function delete()
    {
       $query= "DELETE FROM `hobby` WHERE `hobby`.`id` =".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has been Deleted successfully.
</div>");
            Utility::redirect('index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been deleted  successfully.
    </div>");
            Utility::redirect('index.php');

        }
    }

    public function trash()
    {
        $this->deleted_at=time();

        $query= "UPDATE `hobby` SET `deleted_at` = '{$this->deleted_at}' WHERE `hobby`.`id` =".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-info\">
  <strong>trashed!</strong> Data has been trashed successfully.
</div>");
            Utility::redirect('index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been deleted  successfully.
    </div>");
            Utility::redirect('index.php');

        }
    }

    public function trashed()
    {
        $allhobby= array();
        $query= "SELECT * FROM `hobby` WHERE `deleted_at` IS NOT NULL ";
        $result= mysqli_query($this->conn,$query);

        while ($row=mysqli_fetch_assoc($result)){
            $allhobby[] = $row;
        }

        return $allhobby;

    }


    public function recover(){
        $query="UPDATE `atomicproject`.`hobby` SET `deleted_at` = NULL  WHERE `hobby`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
  <strong>Recovered!</strong> Data has been recovered successfully.
</div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been recovered successfully.
    </div>");
            Utility::redirect('index.php');

        }

    }

    public function deleteMultiple($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "DELETE FROM `atomicproject`.`hobby` WHERE `hobby`.`id`  IN(" . $ids . ")";
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has been deleted successfully.
</div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has not been deleted successfully.
</div>");
                Utility::redirect("index.php");
            }

        }
    }

    public function recoverMultiple($idS=array()){
        if((is_array($idS)) && count($idS)>0){
            $IDs= implode(",",$idS);
            $query="UPDATE `atomicproject`.`hobby` SET `deleted_at` = NULL  WHERE `hobby`.`id` IN(".$IDs.")";
            //result= mysqli_query($this->conn,$query);
            $result= mysqli_query($this->conn,$query);
            if($result){
                Message::message("<div class=\"alert alert-success\">
  <strong>Recovered!</strong> Selected Data has been recovered successfully.
</div>");
                header('Location:index.php');

            } else {
                Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Selected Data has not been recovered successfully.
    </div>");
                Utility::redirect('index.php');

            }



        }

    }

    public function count(){
        $query="SELECT COUNT(*) AS totalItem FROM `hobby`";
        $result=mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];

    }

    public function paginator($pageStartFrom=0,$Limit=5){
        $query="SELECT * FROM `hobby` LIMIT ".$pageStartFrom.",".$Limit;
        $_allBook= array();
        $result= mysqli_query($this->conn,$query);
        //You can also use mysqli_fetch_object e.g: $row= mysqli_fetch_object($result)
        while($row= mysqli_fetch_object($result)){
            $_allBook[]=$row;
        }

        return $_allBook;

    }

}