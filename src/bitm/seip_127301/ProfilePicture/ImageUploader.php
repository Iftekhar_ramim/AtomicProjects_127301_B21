<?php

namespace App\bitm\seip_127301\ProfilePicture;
use App\bitm\seip_127301\message\Message;
use App\bitm\seip_127301\utility\Utility;

class ImageUploader{
    public $id="";
    public $name="";
    public $image_name="";
    public $conn;
    public $deleted_at='';



    public function __construct()
    {
        $this->conn= mysqli_connect("localhost","root","","atomicproject") or die("Database connection failed");
    }

    public function prepare($data=array()){
        if(array_key_exists("name",$data)){
            $this->name=  filter_var($data['name'], FILTER_SANITIZE_STRING);
        }
        if(array_key_exists("image",$data)){
            $this->image_name=  filter_var($data['image'], FILTER_SANITIZE_STRING);
        }
        if(array_key_exists("id",$data)){
            $this->id= $data['id'];
        }

        return $this;

    }

    public function store(){
        $query="INSERT INTO `atomicproject`.`profilepicture` ( `name`, `images`) VALUES ('{$this->name}', '{$this->image_name}')";
      // echo $query;

        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-info\">
            <strong>Inserted!</strong> Data has been Inserted successfully.
        </div>");
            Utility::redirect('index.php');

        }else{
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been Inserted .
    </div>");
            Utility::redirect('index.php');
        }
    }
    public function index(){
        $_allInfo= array();
        $query="SELECT * FROM `profilepicture` WHERE `deleted_at` IS NULL";
        $result= mysqli_query($this->conn,$query);
        //You can also use mysqli_fetch_object e.g: $row= mysqli_fetch_object($result)
        while($row= mysqli_fetch_assoc($result)){
            $_allInfo[]=$row;
        }

        return $_allInfo;
    }

    public function view(){
        $query="SELECT * FROM `profilepicture` WHERE `id`=".$this->id;
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row;
    }
    public function update(){
        $query="UPDATE `atomicproject`.`profilepicture` SET `name` = '{$this->name}', `images` = '{$this->image_name}' WHERE `profilepicture`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-info\">
  <strong>Updated!</strong> Data has been Updated successfully.
</div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been updated  successfully.
    </div>");
            Utility::redirect('index.php');

        }

    }

    public function delete(){
        $query="DELETE FROM `atomicproject`.`profilepicture` WHERE `profilepicture`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
  <strong>Deleted!</strong> Data has been deleted successfully.
</div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been deleted successfully.
    </div>");
            Utility::redirect('index.php');

        }
    }

    public function trash(){
        $this->deleted_at=time();
        $query= "UPDATE `profilepicture` SET `deleted_at` = '".$this->deleted_at."' WHERE `profilepicture`.`id` =".$this->id;
        $result = $this->conn->query($query);
        if($result){
            Message::message("<div class=\"alert alert-info\">
            <strong>Trashed!</strong> Data has been trashed successfully.
        </div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been trashed successfully.
    </div>");
            Utility::redirect('index.php');

        }

    }

    public function trashed(){

        $_allprofilepicture = array();
        $query= "SELECT * FROM `profilepicture`  WHERE `deleted_at` IS  Not NULL";
        $result= mysqli_query($this->conn,$query);
        while($row= mysqli_fetch_assoc($result)){
            $_allprofilepicture[]=$row;
        }

        return $_allprofilepicture;

    }

    public function recover(){
        $query="UPDATE `atomicproject`.`profilepicture` SET `deleted_at` = NULL  WHERE `profilepicture`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
  <strong>Recovered!</strong> Data has been recovered successfully.
</div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been recovered successfully.
    </div>");
            Utility::redirect('index.php');

        }

    }

    public function deleteMultiple($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "DELETE FROM `atomicproject`.`profilepicture` WHERE `profilepicture`.`id`  IN(" . $ids . ")";
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has been deleted successfully.
</div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has not been deleted successfully.
</div>");
                Utility::redirect("index.php");
            }

        }
    }

    public function recoverMultiple($idS=array()){
        if((is_array($idS)) && count($idS)>0){
            $IDs= implode(",",$idS);
            $query="UPDATE `atomicproject`.`profilepicture` SET `deleted_at` = NULL  WHERE `profilepicture`.`id` IN(".$IDs.")";
            //result= mysqli_query($this->conn,$query);
            $result= mysqli_query($this->conn,$query);
            if($result){
                Message::message("<div class=\"alert alert-success\">
  <strong>Recovered!</strong> Selected Data has been recovered successfully.
</div>");
                header('Location:index.php');

            } else {
                Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Selected Data has not been recovered successfully.
    </div>");
                Utility::redirect('index.php');

            }



        }

    }

    public function count(){
        $query="SELECT COUNT(*) AS totalItem FROM `profilepicture`";
        $result=mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];

    }

    public function paginator($pageStartFrom=0,$Limit=5){
        $query="SELECT * FROM `profilepicture` WHERE `deleted_at` IS NULL LIMIT ".$pageStartFrom.",".$Limit;
        $_allBook= array();
        $result= mysqli_query($this->conn,$query);
        //You can also use mysqli_fetch_object e.g: $row= mysqli_fetch_object($result)
        while($row= mysqli_fetch_assoc($result)){
            $_allBook[]=$row;
        }

        return $_allBook;

    }








}