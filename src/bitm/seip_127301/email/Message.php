<?php

namespace App\bitm\seip_127301\email;

if (!isset($_SESSION['message'])){
    session_start();
}

class Message {
    public static function message ($message= NUll){
        if (is_null($message)){
            $_message= self::getmessage();
            return $_message;
        }
        else{
            self::setmessage($message);
        }
    }

    public static function setmessage ($message){
        $_SESSION['message']=$message;
    }

    public static function getmessage (){
        $_message= $_SESSION['message'];
        $_SESSION['message']="";
        return $_message;
    }
}