<?php

namespace App\bitm\seip_127301\textarea;
use App\bitm\seip_127301\message\Message;
use App\Bitm\seip_127301\utility\Utility;

class Textarea{
    public $id='';
    public $name='';
    public $textarea='';
    public $conn='';
    public $deleted_at='';
    public $created='';
    public $modified='';
    public $created_by='';
    public $modified_by='';



    public function __construct()
    {
        $this->conn = new \mysqli("localhost","root","","atomicproject") or die("Database Connection Establish Fail.");
    }

    public function index()
    {
        $_alltextarea = array();
        $query= "SELECT * FROM `textarea`  WHERE `deleted_at` IS NULL";
        $result= mysqli_query($this->conn,$query);
        while($row= mysqli_fetch_assoc($result)){
            $_alltextarea[]=$row;
        }

        return $_alltextarea;
    }

    public function view()
    {
        $query= "SELECT * FROM `textarea` WHERE `id`=".$this->id;
        $result= mysqli_query($this->conn,$query);
        //Utility::dd($result);
        $row= mysqli_fetch_assoc($result);
        return $row;
    }




    public function update()
    {
        $query="UPDATE `atomicproject`.`textarea` SET `name` = '{$this->name}', `textarea` = '".$this->textarea."' WHERE `textarea`.`id` =".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-info\">
            <strong>Updated!</strong> Data has been Updated successfully.
        </div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been updated  successfully.
    </div>");
            Utility::redirect('index.php');

        }
    }

    public function delete()
    {
        $query="DELETE FROM `textarea` WHERE `textarea`.`id` =".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-info\">
            <strong>Updated!</strong> Data has been Deleted successfully.
        </div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been Deleted successfully.
    </div>");
            Utility::redirect('index.php');

        }
    }

    public function prepare($data=array()){
        if(array_key_exists("textarea",$data)){
            $this->textarea = $data["textarea"];
        }
        if(array_key_exists("name",$data)){
            $this->name = $data["name"];
        }
        if(array_key_exists("id",$data)){
            $this->id= $data['id'];
        }

        return $this;
    }

    public function store(){
        $query = "INSERT INTO `textarea` ( `name`, `textarea`, `deleted_at`) VALUES ('{$this->name}', '{$this->textarea}', NULL)";

        $result = $this->conn->query($query);
        if($result){
            Message::message("<div class=\"alert alert-info\">
            <strong>Inserted!</strong> Data has been Inserted successfully.
        </div>");

            Utility::redirect('index.php');
        }
        else{
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been Inserted .
    </div>");
            Utility::redirect('index.php');
        }
    }

    public function trash(){
        $this->deleted_at=time();
        $query= "UPDATE `textarea` SET `deleted_at` = '".$this->deleted_at."' WHERE `textarea`.`id` =".$this->id;
        $result = $this->conn->query($query);
        if($result){
            Message::message("<div class=\"alert alert-info\">
            <strong>Updated!</strong> Data has been trashed successfully.
        </div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been trashed successfully.
    </div>");
            Utility::redirect('index.php');

        }

    }

    public function trashed(){

        $_alltextarea = array();
        $query= "SELECT * FROM `textarea`  WHERE `deleted_at` IS  Not NULL";
        $result= mysqli_query($this->conn,$query);
        while($row= mysqli_fetch_assoc($result)){
            $_alltextarea[]=$row;
        }

        return $_alltextarea;

    }

    public function recover(){
        $query="UPDATE `atomicproject`.`textarea` SET `deleted_at` = NULL  WHERE `textarea`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
  <strong>Recovered!</strong> Data has been recovered successfully.
</div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been recovered successfully.
    </div>");
            Utility::redirect('index.php');

        }

    }

    public function deleteMultiple($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "DELETE FROM `atomicproject`.`textarea` WHERE `textarea`.`id`  IN(" . $ids . ")";
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has been deleted successfully.
</div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has not been deleted successfully.
</div>");
                Utility::redirect("index.php");
            }

        }
    }

    public function recoverMultiple($idS=array()){
        if((is_array($idS)) && count($idS)>0){
            $IDs= implode(",",$idS);
            $query="UPDATE `atomicproject`.`textarea` SET `deleted_at` = NULL  WHERE `textarea`.`id` IN(".$IDs.")";
            //result= mysqli_query($this->conn,$query);
            $result= mysqli_query($this->conn,$query);
            if($result){
                Message::message("<div class=\"alert alert-success\">
  <strong>Recovered!</strong> Selected Data has been recovered successfully.
</div>");
                header('Location:index.php');

            } else {
                Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Selected Data has not been recovered successfully.
    </div>");
                Utility::redirect('index.php');

            }



        }

    }

    public function count(){
        $query="SELECT COUNT(*) AS totalItem FROM `textarea`";
        $result=mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];

    }

    public function paginator($pageStartFrom=0,$Limit=5){
        $query="SELECT * FROM `textarea` LIMIT ".$pageStartFrom.",".$Limit;
        $_allBook= array();
        $result= mysqli_query($this->conn,$query);
        //You can also use mysqli_fetch_object e.g: $row= mysqli_fetch_object($result)
        while($row= mysqli_fetch_assoc($result)){
            $_allBook[]=$row;
        }

        return $_allBook;

    }

}