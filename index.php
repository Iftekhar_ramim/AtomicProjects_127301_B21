<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Theme Made By www.w3schools.com - No Copyright -->
  <title>AtomicProjects Index</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <link href="http://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link href="http://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <style>
  body {
      font: 400 15px Lato, sans-serif;
      line-height: 1.8;
      color: #818181;
  }
  h2 {
      font-size: 24px;
      text-transform: uppercase;
      color: #303030;
      font-weight: 600;
      margin-bottom: 30px;
  }
  h4 {
      font-size: 19px;
      line-height: 1.375em;
      color: #303030;
      font-weight: 400;
      margin-bottom: 30px;
  }
  .jumbotron {
      background-color: slateblue;
      color: #fff;
      padding: 100px 25px;
      font-family: Montserrat, sans-serif;
  }
  .container-fluid {
      padding: 60px 50px;
  }
  .bg-grey {
      background-color: #f6f6f6;
  }
  .logo-small {
      color: slateblue;
      font-size: 50px;
  }
  .logo {
      color: orangered;
      font-size: 200px;
  }
  .thumbnail {
      padding: 0 0 15px 0;
      border: none;
      border-radius: 0;
  }
  .thumbnail img {
      width: 100%;
      height: 100%;
      margin-bottom: 10px;
  }
  .carousel-control.right, .carousel-control.left {
      background-image: none;
      color: slateblue;
  }
  .carousel-indicators li {
      border-color: slateblue;
  }
  .carousel-indicators li.active {
      background-color: slateblue;
  }
  .item h4 {
      font-size: 19px;
      line-height: 1.375em;
      font-weight: 400;
      font-style: italic;
      margin: 70px 0;
  }
  .item span {
      font-style: normal;
  }
  .panel {
      border: 1px solid slateblue;
      border-radius:0 !important;
      transition: box-shadow 0.5s;
  }
  .panel:hover {
      box-shadow: 5px 0px 40px rgba(0,0,0, .2);
  }
  .panel-footer .btn:hover {
      border: 1px solid slateblue;
      background-color: #fff !important;
      color: slateblue;
  }
  .panel-heading {
      color: #fff !important;
      background-color: slateblue !important;
      padding: 25px;
      border-bottom: 1px solid transparent;
      border-top-left-radius: 0px;
      border-top-right-radius: 0px;
      border-bottom-left-radius: 0px;
      border-bottom-right-radius: 0px;
  }
  .panel-footer {
      background-color: white !important;
  }
  .panel-footer h3 {
      font-size: 32px;
  }
  .panel-footer h4 {
      color: #aaa;
      font-size: 14px;
  }
  .panel-footer .btn {
      margin: 15px 0;
      background-color: slateblue;
      color: #fff;
  }
  .navbar {
      margin-bottom: 0;
      background-color: slateblue;
      z-index: 9999;
      border: 0;
      font-size: 12px !important;
      line-height: 1.42857143 !important;
      letter-spacing: 4px;
      border-radius: 0;
      font-family: Montserrat, sans-serif;
  }
  .navbar li a, .navbar .navbar-brand {
      color: #fff !important;
  }
  .navbar-nav li a:hover, .navbar-nav li.active a {
      color: slateblue !important;
      background-color: #fff !important;
  }
  .navbar-default .navbar-toggle {
      border-color: transparent;
      color: #fff !important;
  }
  footer .glyphicon {
      font-size: 20px;
      margin-bottom: 20px;
      color: slateblue;
  }
  .slideanim {visibility:hidden;}
  .slide {
      animation-name: slide;
      -webkit-animation-name: slide;
      animation-duration: 1s;
      -webkit-animation-duration: 1s;
      visibility: visible;
  }
  @keyframes slide {
    0% {
      opacity: 0;
      -webkit-transform: translateY(70%);
    }
    100% {
      opacity: 1;
      -webkit-transform: translateY(0%);
    }
  }
  @-webkit-keyframes slide {
    0% {
      opacity: 0;
      -webkit-transform: translateY(70%);
    }
    100% {
      opacity: 1;
      -webkit-transform: translateY(0%);
    }
  }
  @media screen and (max-width: 768px) {
    .col-sm-4 {
      text-align: center;
      margin: 25px 0;
    }
    .btn-lg {
        width: 100%;
        margin-bottom: 35px;
    }
  }
  @media screen and (max-width: 480px) {
    .logo {
        font-size: 150px;
    }
  }
  </style>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#Booktitle">BOOKTITLE</a></li>
        <li><a href="#Email">EMAIL</a></li>
        <li><a href="#Checkbox">CHECKBOX</a></li>
        <li><a href="#Birthday">BIRTHDAY</a></li>
        <li><a href="#Radio">RADIO</a></li>
        <li><a href="#City">CITY</a></li>
        <li><a href="#Picture">PICTURE</a></li>
        <li><a href="#Summary">SUMMARY</a></li>
      </ul>
    </div>
  </div>
</nav>

<div class="jumbotron text-center" style="background-color: slateblue">
  <h1>BITM</h1>
  <h3>Atomic Projects</h3><br>
  <h3 align="right" style="padding-right: 50px">- Prepared By Iftekharul Islam ( 127301, B21 )</h3>
  <h3 align="right" style="padding-right: 100px">- Supervised By Md. Yameen Hossain</h3>
</div>

<!-- Container (About Section) -->
<div id="Booktitle" class="container-fluid">
  <div class="row">
    <div class="col-sm-8">
      <h2>About Book Title</h2><br>
      <ul class="list-group">
        <li class="list-group-item">As a user I want to see the list of Book title so that I know the Available Books. (Rm)</li>
        <li class="list-group-item">As a user I want to add Book title so that new books can be shown on the list.(C)</li>
        <li class="list-group-item">As a user I want to edit any Book title so that I can correct any mistakes (U) </li>
        <li class="list-group-item">As a user I want to view the title of Book individually (Rs)  </li>
        <li class="list-group-item">As a user I want to delete any Book title from the list. So that I can remove any Book the list, if necessary (D)  </li>
      </ul>
      <br><a href="views/Seip_127301/Book/index.php" <button class="btn btn-primary btn-lg">Lets Go to Project</button></a><br>


    </div>
    <div class="col-sm-4"><br><br><br><br>
      <span class="glyphicon glyphicon-book logo"></span>
      <br>
    </div>
  </div>
</div>

<div id="Email" class="container-fluid">
  <div class="row">
    <div class="col-sm-8">
      <h2>About Email</h2><br>
      <ul class="list-group">
        <li class="list-group-item">As an admin I want to let users to subscribe to my website so that I can see the list of all subscribers. (RM)</li>
        <li class="list-group-item">As an admin I want to add anyone else email for subscription ( C )</li>
        <li class="list-group-item">As an admin I want to view email address of individual user. (RS) </li>
        <li class="list-group-item">As an admin I want to edit anyone else email address if required (U)  </li>
        <li class="list-group-item">As an admin I want to delete any email address from the subscription list.</li>
      <br><a href="views/Seip_127301/email/index.php" <button class="btn btn-primary btn-lg">Lets Go to Project</button></a>
    </div>
    <div class="col-sm-4"><br><br><br><br>
      <span class="glyphicon glyphicon-envelope logo"></span>
      <br>
    </div>
  </div>
</div>

<div id="Checkbox" class="container-fluid">
  <div class="row">
    <div class="col-sm-8">
      <h2>About Checkbox </h2><br>
      <ul class="list-group">
        <li class="list-group-item">As a user I want to see the list of hobby of users. (RM)</li>
        <li class="list-group-item">	As a user I want to add my hobbies so that anyone can visit to see my hobbies. (C)</li>
        <li class="list-group-item">	As a user I want to edit/change my hobbies so that I can updates my hobby.</li>
        <li class="list-group-item">	As a user I want to delete my hobbies.</li>


      </ul>
      <br><a href="views/Seip_127301/hobby/index.php" <button class="btn btn-primary btn-lg">Lets Go to Project</button></a>
    </div>
    <div class="col-sm-4"><br><br><br><br>
      <span class="glyphicon glyphicon-check logo"></span>
      <br>
    </div>
  </div>
</div>

<div id="Birthday" class="container-fluid">
  <div class="row">
    <div class="col-sm-8">
      <h2>About Birthday</h2><br>
      <ul class="list-group">
        <li class="list-group-item">As a user I want to add my birthday so that anyone can visit to see the list of all users’ birthdays. (CR )</li>
        <li class="list-group-item">I want to edit anyone else birthday (U)</li>
        <li class="list-group-item">As a user I want to view the birthday of individual user. (RS)  </li>
        <li class="list-group-item">I want to delete anyone else birthday (D) </li>

      </ul>
      <br><a href="views/Seip_127301/date/index.php" <button class="btn btn-primary btn-lg">Lets Go to Project</button></a>
    </div>
    <div class="col-sm-4"><br><br><br><br>
      <span class="glyphicon glyphicon-calendar logo"></span>
      <br>
    </div>
  </div>
</div>

<div id="Radio" class="container-fluid">
  <div class="row">
    <div class="col-sm-8">
      <h2>About Radio</h2><br>
      <ul class="list-group">
        <li class="list-group-item">As  a admin I want to know if a user is MALE/FEMALE so that I can run promotion based on gender.</li>


      </ul>
      <br><a href="views/Seip_127301/radio/index.php" <button class="btn btn-primary btn-lg">Lets Go to Project</button></a>
    </div>
    <div class="col-sm-4"><br><br><br>
      <span class="glyphicon glyphicon-record logo"></span>
      <br>
    </div>
  </div>
</div>

<div id="City" class="container-fluid">
  <div class="row">
    <div class="col-sm-8">
      <h2>About City</h2><br>
      <ul class="list-group">
        <li class="list-group-item">As a user I want to see “City” list of all users. (RM)</li>
        <li class="list-group-item">As a user I want to add city list so that anyone can visit to see the city list. (C )</li>
        <li class="list-group-item">As a user I want to Edit/Chagne city list if needed.  </li>
        <li class="list-group-item">As a user I want to delete my city from the list.)  </li>

      </ul>
      <br><a href="views/Seip_127301/city/index.php" <button class="btn btn-primary btn-lg">Lets Go to Project</button></a>
    </div>
    <div class="col-sm-4"><br><br><br><br>
      <span class="glyphicon glyphicon-home logo"></span>
      <br>
    </div>
  </div>
</div>

<div id="Picture" class="container-fluid">
  <div class="row">
    <div class="col-sm-8">
      <h2>About Picture</h2><br>
      <ul class="list-group">
        <li class="list-group-item">As a user I want to add profile picture so that anyone can visit to my proifle and see my profile picture. (CR)</li>
        <li class="list-group-item">As a user I want to change profile picture so that anyone can see the update picture. (U) </li>
        <li class="list-group-item">As a user I want to delete profle picture (D)  </li>

      </ul>
      <br><a href="views/Seip_127301/ProfilePicture/index.php" <button class="btn btn-primary btn-lg">Lets Go to Project</button></a>
    </div>
    <div class="col-sm-4"><br><br><br>
      <span class="glyphicon glyphicon-user logo"></span>
      <br>
    </div>
  </div>
</div>

<div id="Summary" class="container-fluid">
  <div class="row">
    <div class="col-sm-8">
      <h2>About Summary</h2><br>
      <ul class="list-group">
        <li class="list-group-item">As a user I want to see the list of Summary of Organizations. (Rm)</li>
        <li class="list-group-item">As a user I want to add Summary(C)</li>
        <li class="list-group-item">As a user I want to edit any Summary so that I can correct any mistakes (U) </li>
        <li class="list-group-item">As a user I want to view the Summary individually (Rs)  </li>
        <li class="list-group-item">As a user I want to delete Summary  </li>
      </ul>
      <br><a href="views/Seip_127301/textarea/index.php" <button class="btn btn-primary btn-lg">Lets Go to Project</button></a>
    </div>
    <div class="col-sm-4"><br><br><br><br>
      <span class="glyphicon glyphicon-globe logo"></span>
      <br>
    </div>
  </div>
</div>




<!-- Add Google Maps -->
<script src="http://maps.googleapis.com/maps/api/js"></script>
<script>
var myCenter = new google.maps.LatLng(41.878114, -87.629798);

function initialize() {
var mapProp = {
  center:myCenter,
  zoom:12,
  scrollwheel:false,
  draggable:false,
  mapTypeId:google.maps.MapTypeId.ROADMAP
  };

var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);

var marker = new google.maps.Marker({
  position:myCenter,
  });

marker.setMap(map);
}

google.maps.event.addDomListener(window, 'load', initialize);
</script>

<footer class="container-fluid text-center">
  <a href="#myPage" title="To Top">
    <span class="glyphicon glyphicon-chevron-up"></span>
  </a>

</footer>

<script>
$(document).ready(function(){
  // Add smooth scrolling to all links in navbar + footer link
  $(".navbar a, footer a[href='#myPage']").on('click', function(event) {
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 900, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
  
  $(window).scroll(function() {
    $(".slideanim").each(function(){
      var pos = $(this).offset().top;

      var winTop = $(window).scrollTop();
        if (pos < winTop + 600) {
          $(this).addClass("slide");
        }
    });
  });
})
</script>

</body>
</html>

