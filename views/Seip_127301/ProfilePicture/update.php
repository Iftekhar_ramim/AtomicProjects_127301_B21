<?php
include_once ('../../../vendor/autoload.php');
use App\Bitm\seip_127301\ProfilePicture\ImageUploader;
use App\Bitm\seip_127301\utility\Utility;
use App\Bitm\seip_127301\message\Message;


//Utility::d($_POST);

$profilePicture= new ImageUploader();
$singleInfo= $profilePicture->prepare($_POST)->view();

if((isset($_FILES['image'])) && (!empty($_FILES['image']['name']))){
    $imageName= time(). $_FILES['image']['name'];
    $temporary_location= $_FILES['image']['tmp_name'];
    unlink($_SERVER['DOCUMENT_ROOT'].'/AtomicProjects/resource/Images/'.$singleInfo['images']);
    move_uploaded_file($temporary_location,'../../../resource/Images/'.$imageName);


    $_POST['image']=$imageName;


}
$profilePicture= new ImageUploader();
$profilePicture->prepare($_POST)->update();

//Utility::d($_POST);
