<?php
include_once ('../../../vendor/autoload.php');
use App\bitm\seip_127301\ProfilePicture\ImageUploader;
use App\Bitm\seip_127301\utility\Utility;
use App\Bitm\seip_127301\message\Message;
//Utility::dd($_POST);


//Utility::d($_FILES);

if((isset($_FILES['image'])) && (!empty($_FILES['image']['name']))){
    $imageName= time(). $_FILES['image']['name'];
    $temporary_location= $_FILES['image']['tmp_name'];

    move_uploaded_file($temporary_location,'../../../resource/Images/'.$imageName);


    $_POST['image']=$imageName;


}
$profilePicture= new ImageUploader();
$profilePicture->prepare($_POST)->store();

//Utility::d($_POST);
