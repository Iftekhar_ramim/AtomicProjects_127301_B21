<?php
include_once ('../../../vendor/autoload.php');

use App\Bitm\seip_127301\ProfilePicture\ImageUploader;
use App\Bitm\seip_127301\utility\Utility;
use App\Bitm\seip_127301\message\Message;


$profilePicture= new ImageUploader();
$singleInfo= $profilePicture->prepare($_GET)->view();


?>






<!DOCTYPE html>
<html lang="en">
<head>
    <title>Profile Picture</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<br>
<div class="row" align="center">
    <div class="btn-group" >
        <?php echo '<a href= "../../../" class="btn btn-success" role="button" >INDEX</a>'?>
        <?php echo '<a href="index.php" class="btn btn-primary" role="button" >Home</a>'?>
        <?php echo '<a href="create.php" class="btn btn-info" role="button" >Add </a>'?>
        <?php echo '<a href="edit.php" class="btn btn-warning" role="button" >View</a>'?>
        <?php echo'<a href="trashed_view.php" class="btn btn-danger" role="button" >view all trashes</a>'?>

    </div>
</div>

<br>

<div class="container">
    <h2>Edit Profile</h2>
    <form role="form" action="update.php" method="post" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?php echo $singleInfo['id']?>">
        <div class="form-group">
            <label>Name:</label>
            <input type="text" class="form-control"  placeholder="Enter your name" name="name" value="<?php echo $singleInfo['name']?>">
        </div>
        <div class="form-group">
            <label for="pwd">Upload file:</label>
            <input type="file" class="form-control" name="image">
            <img src="../../../resource/Images/<?php echo $singleInfo['images']?>" alt="image" height="100px" width="100px" class="img-responsive">
            <?php echo $singleInfo['images']?>
        </div>

        <input type="submit" class="btn btn-default" value="Submit">
    </form>
</div>

</body>
</html>


