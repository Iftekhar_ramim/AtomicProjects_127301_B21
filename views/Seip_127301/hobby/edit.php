<?php

session_start();
include_once('../../../vendor/autoload.php.');

use App\bitm\seip_127301\hobby\Hobby;
use App\bitm\seip_127301\message\Message;
use App\Bitm\seip_127301\utility\Utility;


$hobby = new Hobby();
$singlerow =$hobby->prepare($_GET)->view();

$singlehobbylist= $singlerow['hobby'];
$singlehobbyarray= explode(',',$singlehobbylist);

//Utility::dd($singlehobbyarray);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Hobby</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
<br>
<div class="row" align="center">
    <div class="btn-group" >
        <?php echo '<a href= "../../../" class="btn btn-success" role="button" >INDEX</a>'?>
        <?php echo '<a href="index.php" class="btn btn-primary" role="button" >Home</a>'?>
        <?php echo '<a href="create.php" class="btn btn-info" role="button" >Add </a>'?>
        <?php echo '<a href="edit.php" class="btn btn-warning" role="button" >View</a>'?>
        <?php echo'<a href="trashed_view.php" class="btn btn-danger" role="button" >view all trashes</a>'?>

    </div>
</div>

<br>

<div class="container">
    <h2>Select your hobby</h2>

    <form role="form" action="update.php" method="post">
        <div class="form-group">
            <input type="hidden" name="id" value="<?php echo $singlerow['id']?>">
            <label>Name:</label>
            <input type="text" name="name" class="form-control" style="width: 50%" value="<?php echo $singlerow['name']?>">
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="Hobby[]" value="Playing Cricket" <?php if(in_array('Playing Cricket',$singlehobbyarray)){echo "checked";} else {echo "";}?>>Playing Cricket</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="Hobby[]" value="Coding" <?php if(in_array('Coding',$singlehobbyarray)){echo "checked";} else {echo "";}?>>Coding</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="Hobby[]" value="Browsing" <?php if(in_array('Browsing',$singlehobbyarray)){echo "checked";} else {echo "";}?>>Browsing</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="Hobby[]" value="Book reading" <?php if(in_array('Book reading',$singlehobbyarray)){echo "checked";} else {echo "";}?>>Book reading</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="Hobby[]" value="Gardening" <?php if(in_array('Gardening',$singlehobbyarray)){echo "checked";} else {echo "";}?>>Gardening</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="Hobby[]" value="Teaching" <?php if(in_array('Teaching',$singlehobbyarray)){echo "checked";} else {echo "";}?>>Teaching</label>
        </div>
        <input type="submit" value="Submit">
    </form>
</div>

</body>
</html>


