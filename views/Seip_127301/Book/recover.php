<?php
include_once ('../../../vendor/autoload.php');
use App\bitm\seip_127301\book\Book;

$book = new Book();
$book->prepare($_GET)->recover();