<!DOCTYPE html>

<?php
include_once('../../../vendor/autoload.php');

use App\bitm\seip_127301\book\Book;


$book= new Book();
$singleItem= $book->prepare($_GET)->view();
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body >

<div class="container">
    <h3 align="center" style="color: blueviolet"> Update Book Title</h3>

    <br>
    <br>


    <div class="row" align="right">
        <div class="btn-group" >
            <?php echo '<a href= "../../../" class="btn btn-success" role="button" >INDEX</a>'?>
            <?php echo '<a href="index.php" class="btn btn-primary" role="button" >Home</a>'?>
            <?php echo '<a href="create.php" class="btn btn-info" role="button" >Add </a>'?>
            <?php echo '<a href="edit.php" class="btn btn-warning" role="button" >View</a>'?>
            <?php echo'<a href="trashed_view.php" class="btn btn-danger" role="button" >view all trashes</a>'?>

        </div>
    </div>

    <br>
    <br>

    <form role="form" action="update.php" method="post"  align="center" style="width:50%">
        <div class="form-group">
            <label>Update Book title:</label>
            <input type="hidden" name="id"   value="<?php echo $singleItem->id?>">
            <input type="text" name="title" class="form-control" id="email" value="<?php echo $singleItem->bookTitle?>">
        </div>

        <button type="submit" class="btn btn-default">Update</button>
    </form>

</div>


</body>
</html>


