<!DOCTYPE html>

<?php

include_once('../../../vendor/autoload.php');

use App\bitm\seip_127301\book\Book;
use App\bitm\seip_127301\utility\Utility;

$book= new Book();
$singleData=$book->prepare($_GET)->view();
//Utility::d($singleData);
?>


<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h3 align="center" style="color: darkblue"> View Details</h3>

    <br>
    <br>


    <div class="row" align="right">
        <div class="btn-group" >
            <?php echo '<a href= "../../../" class="btn btn-success" role="button" >INDEX</a>'?>
            <?php echo '<a href="index.php" class="btn btn-primary" role="button" >Home</a>'?>
            <?php echo '<a href="create.php" class="btn btn-info" role="button" >Add </a>'?>
            <?php echo '<a href="edit.php" class="btn btn-warning" role="button" >Edit</a>'?>
            <?php echo'<a href="trashed_view.php" class="btn btn-danger" role="button" >view all trashes</a>'?>

        </div>
    </div>

    <br>
    <br>

    <div class="container" align="center">
        <h2><?php echo $singleData->bookTitle ?> </h2>
        <ul class="list-group" style="width:50%" >
            <li class="list-group-item"><strong>ID :</strong> <?php echo $singleData->id?> </li>
            <li class="list-group-item"><strong>Book Title :</strong> <?php echo $singleData->bookTitle ?> </li>

        </ul>
    </div>

    <div align="center" >
        <a href="index.html" class="btn btn-default" role="button" >Back</a>

    </div>
</div>


</body>


</html>

