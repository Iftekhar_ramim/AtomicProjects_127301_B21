<!DOCTYPE html>
<html lang="eng">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Add city</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/js/bootstrap.js">
</head>
<body>
<form role="form" action="store.php" method="post">
    <div class="form-group">
        <label for="pwd">Name:</label>
        <input type="text" name="name" class="form-control" id="pwd" style="width: 50%"> <br><br>
        
        <label for="sel1">Select City (select one):</label>
      <select class="form-control" id="sel1" name="city" style="width: 50%">
		<option>--select--</option>
        <option value="Dhaka">Dhaka</option>
        <option value="Chittagong">Chittagong</option>
        <option>Khulna</option>
        <option>Rajshahi</option>
		 <option>Barisal</option>
		  <option>Sylhet</option>
      </select>
    </div>
    <button type="submit" class="btn btn-default">Submit</button>
</form>
</body>
</html>