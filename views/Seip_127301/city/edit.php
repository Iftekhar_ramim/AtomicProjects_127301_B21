<?php

session_start();
include_once('../../../vendor/autoload.php.');

use App\bitm\seip_127301\city\City;
use App\bitm\seip_127301\message\Message;
use App\Bitm\seip_127301\utility\Utility;



$city= new City ();
$singleItem= $city->prepare($_GET)->view();


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Update city</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<br>
<div class="row" align="center">
    <div class="btn-group" >
        <?php echo '<a href= "../../../" class="btn btn-success" role="button" >INDEX</a>'?>
        <?php echo '<a href="index.php" class="btn btn-primary" role="button" >Home</a>'?>
        <?php echo '<a href="create.php" class="btn btn-info" role="button" >Add </a>'?>
        <?php echo '<a href="edit.php" class="btn btn-warning" role="button" >View</a>'?>
        <?php echo'<a href="trashed_view.php" class="btn btn-danger" role="button" >view all trashes</a>'?>

    </div>
</div>

<br>

<div class="container">
    <h2>Atomic Project- City</h2>
    <form role="form" action="update.php" method="post">
        <div class="form-group">
            <label>Update Address:</label><br><br>
            <input type="hidden" name="id"   value="<?php echo $singleItem['id']?>">
			
			  <label for="pwd">Name:</label>
        <input type="text" name="name" class="form-control" id="pwd" value="<?php  echo $singleItem['name'] ?>" style="width: 50%"> <br><br>
             
        <label for="sel1">Update City (select one):</label>
      <select class="form-control" id="sel1" name="city" style="width: 50%">

        <option   <?php if($singleItem['city']=='Dhaka') {echo "selected";} ?> >Dhaka</option>
        <option <?php if($singleItem['city']=='Chittagong') {echo "selected";} else{"";}?> >Chittagong</option>
        <option <?php if($singleItem['city']=='Khulna') {echo "selected";} else{"";}?>>Khulna</option>
        <option <?php if($singleItem['city']=='Rajshahi') {echo "selected";} else{"";}?>>Rajshahi</option>
		 <option <?php if($singleItem['city']=='Barisal') {echo "selected";} else{"";}?>>Barisal</option>
		  <option <?php if($singleItem['city']=='Sylhet') {echo "selected";} else{"";}?>>Sylhet</option>
      </select>
        </div>

        <button type="submit" class="btn btn-default">Update</button>
    </form>
</div>

</body>
</html>