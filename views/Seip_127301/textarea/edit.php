<?php

session_start();
include_once('../../../vendor/autoload.php.');

use App\bitm\seip_127301\textarea\Textarea;
use App\bitm\seip_127301\textarea\Message;
use App\Bitm\seip_127301\textarea\Utility;



$textarea= new Textarea ();
$singleItem= $textarea->prepare($_GET)->view();


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Update </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
<br>
<div class="row" align="center">
    <div class="btn-group" >
        <?php echo '<a href= "../../../" class="btn btn-success" role="button" >INDEX</a>'?>
        <?php echo '<a href="index.php" class="btn btn-primary" role="button" >Home</a>'?>
        <?php echo '<a href="create.php" class="btn btn-info" role="button" >Add </a>'?>
        <?php echo '<a href="edit.php" class="btn btn-warning" role="button" >View</a>'?>
        <?php echo'<a href="trashed_view.php" class="btn btn-danger" role="button" >view all trashes</a>'?>

    </div>
</div>

<br>
<div class="container" align="center">
    <h2>Update</h2>
    <form role="form" action="update.php" method="post">
        <div class="form-group">
              <br>
        <label for="pwd">Company Name:</label>
            <input type="hidden" name="id"   value="<?php echo $singleItem['id']?>">
        <input type="text" name="name" class="form-control" id="pwd" style="width: 50%" value="<?php echo $singleItem['name']?>" ><br><br>
        <label for="comment">Update Summary:</label>
        <textarea class="form-control" name="textarea" rows="5" id="comment" style="width: 70%" value="<?php echo $singleItem['textarea']?>"></textarea>
        </div>

        <button type="submit" class="btn btn-default">Update</button>
    </form>
</div>

</body>
</html>