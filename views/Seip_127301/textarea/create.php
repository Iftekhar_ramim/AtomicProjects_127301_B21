<!DOCTYPE html>
<html lang="eng">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Summary</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/js/bootstrap.js">
</head>
<body>
<div align="center">
<form role="form" action="store.php" method="post">
    <div class="form-group">
        <br>
        <label for="pwd">Company Name:</label>
        <input type="text" name="name" class="form-control" id="pwd" style="width: 50%" ><br><br>
        <label for="comment">Add Summary:</label>
        <textarea class="form-control" name="textarea" rows="5" id="comment" style="width: 70%"></textarea>
    </div>
    <button type="submit" class="btn btn-default">Submit</button>
</form>
</div>
</body>
</html>