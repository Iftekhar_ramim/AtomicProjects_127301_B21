<?php


include_once('../../../vendor/autoload.php.');

use App\bitm\seip_127301\date\Date;
use App\bitm\seip_127301\message\Message;
use App\Bitm\seip_127301\utility\Utility;


if((isset($_POST["date"])) && (!empty($_POST["date"]))) {
    $date = new Date ();
    $date->prepare($_POST)->update();
}

else {
    echo "sorry! field is empty";
}

?>