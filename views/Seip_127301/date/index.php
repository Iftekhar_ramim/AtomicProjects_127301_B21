<?php

session_start();
include_once('../../../vendor/autoload.php.');

use App\bitm\seip_127301\date\Date;
use App\bitm\seip_127301\message\Message;
use App\Bitm\seip_127301\book\Utility;



$book= new Date();
$totalItem=$book->count();
//Utility::dd($totalItem);
if(array_key_exists('itemPerPage',$_SESSION)){
    if(array_key_exists('itemPerPage',$_GET)){
        $_SESSION['itemPerPage']=$_GET['itemPerPage'];
    }
}
else{
    $_SESSION['itemPerPage']=5;
}

$itemPerPage= $_SESSION['itemPerPage'];


$noOfPage= ceil($totalItem/$itemPerPage);
//Utility::d($noOfPage);
$pagination="";
if(array_key_exists('pageNumber',$_GET)){
    $pageNo=$_GET['pageNumber'];
}
else{
    $pageNo=1;
}
for($i=1;$i<=$noOfPage;$i++){
    $active=($pageNo==$i)?"active":"";
    $pagination.="<li class='$active'><a href='index.php?pageNumber=$i'>$i</a></li>";
}

$pageStartFrom=$itemPerPage*($pageNo-1);

$allBook=$book->paginator($pageStartFrom,$itemPerPage);
$prev=$pageNo-1;
$next=$pageNo+1;
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Birthday List</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>

<body>

<div class="container" align="center">
    <h2>Birthday List</h2>

    <div class="row" align="right">
        <div class="btn-group" >
            <?php echo '<a href= "../../../" class="btn btn-success" role="button" >INDEX</a>'?>
            <?php echo '<a href="create.php" class="btn btn-info" role="button" >Add Birthdate</a>'?>
            <?php echo'<a href="trashed_view.php" class="btn btn-danger" role="button" >view all trashes</a>'?>

        </div>
    </div>
    <br><br>

    <div id="message">
        <?php if((array_key_exists('message',$_SESSION)&& !empty($_SESSION['message'])))
        {  echo Message::message(); }?>
    </div>

    <div>
        <form role="form" action="index.php" style="width: 30%">
            <div class="form-group">
                <label for="sel1">Select list (select one):</label>
                <select class="form-control" id="sel1" name="itemPerPage">
                    <option <?php if($itemPerPage==5){echo "selected";}else{"";}?>>5</option>
                    <option <?php if($itemPerPage==10){echo "selected";}else{"";}?>>10</option>
                    <option <?php if($itemPerPage==15){echo "selected";}else{"";}?>>15</option>
                    <option <?php if($itemPerPage==20){echo "selected";}else{"";}?>>20</option>
                    <option <?php if($itemPerPage==25){echo "selected";}else{"";}?>>25</option>
                </select>
                <br>
                <button type="submit">Go!</button>

        </form>
    </div>
    <br>
    <div align="right" style="padding-left: 100px">
        <a href="pdf.php" class="btn btn-default" role="button">Download as PDF</a>
        <a href="xl.php" class="btn btn-default" role="button">Download as XL</a>
        <a href="mail.php" class="btn btn-default" role="button">Email to friend</a>
    </div>
    <br>
    <div class=" table-responsive table-bordered">
        <table class="table table-responsive "  align="center"  >
            <thead>
            <tr>
                <th>SL</th>
                <th>ID</th>
                <th>NAME</th>
                <th>Birthdate</th>
                <th>Action</th>
            </tr>
            </thead>

            <tbody>
            <?php
            $sl=0;
            foreach($allBook as $book){
                $sl++;
                ?>
                <tr <?php if($sl%2) echo 'bgcolor="#F8E0F1"'; else echo 'bgcolor="#f8f8ff"';?>>
                    <td><?php echo $sl;?></td>
                    <td><?php echo $book['id'];?></td>
                    <td><?php echo $book['name'];?></td>
                    <td><?php $var=$book['date'];
                                $dt=date('d-m-Y',strtotime($var));

                        echo $dt; ?></td>
                    <td>
                        <a href="view.php?id=<?php echo $book['id']?>" class="btn btn-info btn-xs" role="button">View</a>
                        <a href="edit.php?id=<?php echo $book['id']?>" class="btn btn-primary  btn-xs" role="button">Edit</a>
                        <a href="delete.php?id=<?php echo $book['id']?>" class="btn btn-danger  btn-xs" role="button">Delete</a>
                        <a href="trash.php?id=<?php echo $book['id']?>" class="btn btn-info  btn-xs" role="button">Trash</a>
                    </td>
                </tr>
            <?php } ?>

            </tbody>
        </table>
    </div>
</div>

<div class="container"  align="center">

    <ul class="pagination">
        <?php if($pageNo>1){echo "<li><a href='index.php?pageNumber=$prev'>Prev</a></li>";}else{echo "";}?>
        <?php echo $pagination?>
        <?php if($pageNo<$noOfPage){echo "<li><a href='index.php?pageNumber=$next'>Next</a></li>";}else{echo "";}?>
    </ul>
</div>

<script>
    $('#message').show().delay(10).fadeOut();
    $('#message').show().delay(10).fadeIn();
    $('#message').show().delay(10).fadeOut();
    $('#message').show().delay(10).fadeIn();
    $('#message').show().delay(1200).fadeOut();
</script>

</body>
</html>
