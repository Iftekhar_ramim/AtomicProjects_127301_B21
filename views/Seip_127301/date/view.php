<?php

session_start();
include_once('../../../vendor/autoload.php.');

use App\bitm\seip_127301\date\Date;
use App\bitm\seip_127301\message\Message;
use App\Bitm\seip_127301\utility\Utility;



$date= new date ();
$singleData= $date->prepare($_GET)->view();


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>View selected date</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<br>

<div class="row" align="center">
    <div class="btn-group" >
        <?php echo '<a href= "../../../" class="btn btn-success" role="button" >INDEX</a>'?>
        <?php echo '<a href="index.php" class="btn btn-primary" role="button" >Home</a>'?>
        <?php echo '<a href="create.php" class="btn btn-info" role="button" >Add </a>'?>
        <?php echo '<a href="edit.php" class="btn btn-warning" role="button" >Edit</a>'?>
        <?php echo'<a href="trashed_view.php" class="btn btn-danger" role="button" >view all trashes</a>'?>

    </div>
</div>

<br>


<div class="container">
    <h2><?php echo $singleData['name'] ?> </h2>
    <ul class="list-group">
        <li class="list-group-item">ID: <?php echo $singleData['id']?> </li>
        <li class="list-group-item">Name: <?php echo $singleData['name']?> </li>
        <li class="list-group-item">Birthdate: <?php echo $singleData['date'] ?> </li>

    </ul>
</div>



</body>
</html>
