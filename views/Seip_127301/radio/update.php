<?php


include_once('../../../vendor/autoload.php.');

use App\bitm\seip_127301\radio\Radio;
use App\bitm\seip_127301\message\Message;
use App\Bitm\seip_127301\utility\Utility;


if((isset($_POST["radio"])) && (!empty($_POST["radio"]))) {
    $radio = new Radio ();
    $radio->prepare($_POST)->update();
}

else {
    echo "sorry! field is empty";
}

?>