<?php


include_once('../../../vendor/autoload.php.');

use App\bitm\seip_127301\email\Email;
use App\bitm\seip_127301\email\Message;
use App\Bitm\seip_127301\email\Utility;


if((isset($_POST["email"])) && (!empty($_POST["email"]))) {
    $email = new Email ();
    $email->prepare($_POST)->update();
}

else {
    echo "sorry! field is empty";
}

?>