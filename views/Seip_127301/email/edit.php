<?php

session_start();
include_once('../../../vendor/autoload.php.');

use App\bitm\seip_127301\email\Email;
use App\bitm\seip_127301\email\Message;
use App\Bitm\seip_127301\email\Utility;



$email= new Email ();
$singleItem= $email->prepare($_GET)->view();


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Update Email</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<br>
<div class="row" align="center">
    <div class="btn-group" >
        <?php echo '<a href= "../../../" class="btn btn-success" role="button" >INDEX</a>'?>
        <?php echo '<a href="index.php" class="btn btn-primary" role="button" >Home</a>'?>
        <?php echo '<a href="create.php" class="btn btn-info" role="button" >Add </a>'?>
        <?php echo '<a href="edit.php" class="btn btn-warning" role="button" >View</a>'?>
        <?php echo'<a href="trashed_view.php" class="btn btn-danger" role="button" >view all trashes</a>'?>

    </div>
</div>

<br>
<div class="container">
    <h2>Atomic Project- Book</h2>
    <form role="form" action="update.php" method="post">
        <div class="form-group">
            <label>Update Email Address:</label><br><br>
            <input type="hidden" name="id"   value="<?php echo $singleItem['id']?>">
            <input type="text" name="name" class="form-control" id="name" value="<?php echo $singleItem['name']?>"><br><br>
            <input type="text" name="email" class="form-control" id="email" value="<?php echo $singleItem['email']?>">
        </div>

        <button type="submit" class="btn btn-default">Update</button>
    </form>
</div>

</body>
</html>